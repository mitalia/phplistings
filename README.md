Quick & dirty PHP directory listings generator; useful to snoop around on servers

- generates file lists for directories, provides the file otherwise;
- canonicalize the given paths;
- patches the MIME type for `text/*` formats to `text/plain` to allow for quick in-browser view of sources for scripts, pages, etc.
