<?php
$p = realpath($_GET['p']);
if(is_file($p))
{
    $ct = mime_content_type($p);
    if(strpos($ct, "text/")===0)
        $ct = 'text/plain';
    header('Content-Type: ' . $ct);
    echo file_get_contents($p);
    exit;
}
else if(is_dir($p))
{
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Listings for <?php echo $p; ?></title>
    </head>
    <body>
        <h1>Listings for <?php echo $p; ?></h1>
        <ul>
<?php
    foreach(scandir($p) as $f)
    {
        $ff = realpath($p . "/" . $f);
?>
            <li><a href="<?php echo basename(__FILE__) . "?p=" . urlencode($ff)?>"><?php
        echo $f . (is_dir($ff)?'/':'')
?>
                </a></li>
<?php
    }
?>
        </ul>
    </body>
</html>
<?php
}
else
{
?>
WTF?
<?php
}
?>
